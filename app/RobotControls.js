'use strict';

export default class RobotControls {

	constructor(robotInstance) {
		if (robotInstance === undefined) { // RobotControls Class must be initialized with a robotInstance
			return false;
		}
		this.robot = robotInstance;
		return this;
	}

	addInstruction(instructionString) {
		let instructions = instructionString.split(' ');
		let instructionFunc = instructions[0];
		let instructionArgs = instructions[1];

		if (!instructionArgs) {
			return this[instructionFunc]();
		} else {
			return this[instructionFunc](instructionArgs.split(','));
		}
	}

	PLACE() {
		let args = Array.prototype.slice.call(arguments)[0]; // [0] cause the initial split(' ') converts them to a nested array item
		let x = Number(args[0]);
		let y = Number(args[1]);
		let facing = args[2];

		this.robot.setFacing(facing);
		this.robot.setPosition(x, y);
	}

	MOVE() {
		this.robot.move();
	}

	LEFT() {
		this.robot.rotate('LEFT');
	}

	RIGHT() {
		this.robot.rotate('RIGHT');
	}

	REPORT() {
		let pos = this.robot.getPosition();
		let facing = this.robot.getFacing();

		return pos.x + ',' + pos.y + ',' + facing; // X,Y,F
	}

	RESET() {
		this.robot.setFacing(undefined, true); // true = overide to allow for an illegal facing
		this.robot.setPosition(undefined, undefined, true); // true = overide to allow for an illegal position
	}

}
