# Install the dependencies
Run the following command within this folder to install a node server, Mocha, and browserify. All items install locally (not globally).

```
npm install
```

# Testing the toy robot app
There are two ways to test the functionality of the toy robot app. Via the terminal with Mocha, or in your browser via your browser's console.

## Test in terminal via Mocha
Type the following command into your terminal.

```
npm test
```

## Test in your browser's console
You can test in your browser's console by typing in the following command and open the inspector (Refresh the window).

```
npm run in_browser
```

Testing in the browser console can be done using the following commands:

```
robotControls.addInstruction('PLACE 0,0,NORTH');

robotControls.addInstruction('MOVE');

robotControls.addInstruction('LEFT');

robotControls.addInstruction('RIGHT');

robotControls.addInstruction('REPORT'); // X,Y,F

robotControls.addInstruction('RESET'); // Resets robot back to initial state
```
