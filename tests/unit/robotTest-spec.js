/*eslint-env node, mocha*/
'use strict';

describe('RobotTest', () => {

	const assert = require('assert');
	const path = require('path');
	const Room = require(path.join(__dirname, '../../app/Room.js'));
	const Robot = require(path.join(__dirname, '../../app/Robot.js'));
	const RobotControls = require(path.join(__dirname, '../../app/RobotControls.js'));
	let room;
	let robot;
	let robotControls;

	beforeEach(() => {
		room = new Room([
			['a1', 'a2', 'a3', 'a4', 'a5'],
			['b1', 'b2', 'b3', 'b4', 'b5'],
			['c1', 'c2', 'c3', 'c4', 'c5'],
			['d1', 'd2', 'd3', 'd4', 'd5'],
			['e1', 'e2', 'e3', 'e4', 'e5']
		]);
		robot = new Robot(room);
		robotControls = new RobotControls(robot);
	});

	afterEach(() => {
		robot = {};
		room = {};
		robotControls = {};
	});

	describe('Test Setup', () => {

		it('Robot, RobotControls and Room should exist', () => {
			assert.equal(typeof(Robot), 'function');
			assert.equal(typeof(RobotControls), 'function');
			assert.equal(typeof(Room), 'function');
		});

		it('Robot initialises with default values', () => {
			assert.equal(robotControls.robot.getFacing(), undefined);
			assert.equal(robotControls.robot.getPosition().x, undefined);
			assert.equal(robotControls.robot.getPosition().y, undefined);
			assert.equal(robotControls.robot.room.getFloorPlan()[1][1], 'd2');
			assert.equal(robotControls.robot.room.getFloorPlan()[2][3], 'c4');
			assert.equal(robotControls.robot.room.getFloorPlan()[4][4], 'a5');
		});
	});

	describe('Robot is controllable', () => {

		it('Room won\'t init without a floor plan', () => {
			assert.equal(new Room().hasOwnProperty('floorPlan'), false);
		});

		it('Robot won\'t init without a room', () => {
			assert.equal(new Robot().hasOwnProperty('facing'), false);
		});

		it('RobotControls won\'t init without a robot', () => {
			assert.equal(new RobotControls().hasOwnProperty('robot'), false);
		});

		it('Robot can turn LEFT in 90 degree increments, 450 degrees', () => {
			robotControls.addInstruction('PLACE 0,0,NORTH');
			robotControls.addInstruction('LEFT');
			assert.equal(robotControls.robot.getFacing(), 'WEST');
			robotControls.addInstruction('LEFT');
			assert.equal(robotControls.robot.getFacing(), 'SOUTH');
			robotControls.addInstruction('LEFT');
			assert.equal(robotControls.robot.getFacing(), 'EAST');
			robotControls.addInstruction('LEFT');
			assert.equal(robotControls.robot.getFacing(), 'NORTH');
			robotControls.addInstruction('LEFT');
			assert.equal(robotControls.robot.getFacing(), 'WEST');
		});

		it('Robot can turn RIGHT in 90 degree increments, 450 degrees', () => {
			robotControls.addInstruction('PLACE 0,0,NORTH');
			robotControls.addInstruction('RIGHT');
			assert.equal(robotControls.robot.getFacing(), 'EAST');
			robotControls.addInstruction('RIGHT');
			assert.equal(robotControls.robot.getFacing(), 'SOUTH');
			robotControls.addInstruction('RIGHT');
			assert.equal(robotControls.robot.getFacing(), 'WEST');
			robotControls.addInstruction('RIGHT');
			assert.equal(robotControls.robot.getFacing(), 'NORTH');
			robotControls.addInstruction('RIGHT');
			assert.equal(robotControls.robot.getFacing(), 'EAST');
		});

		it('Robot can\'t be placed in an illegal position', () => {
			robotControls.addInstruction('PLACE -6,-8,NORTH');
			robotControls.addInstruction('MOVE');
			assert.equal(robotControls.robot.getPosition().x, undefined);
			assert.equal(robotControls.robot.getPosition().y, undefined);
		});

		it('Robot won\'t move before a PLACE command is called', () => {
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('LEFT');
			robotControls.addInstruction('RIGHT');
			robotControls.addInstruction('MOVE');
			assert.equal(robotControls.robot.getPosition().x, undefined);
			assert.equal(robotControls.robot.getPosition().y, undefined);
			assert.equal(robotControls.robot.getFacing(), 'NORTH');

			robotControls.addInstruction('PLACE 0,0,NORTH');
			robotControls.addInstruction('MOVE');
			assert.equal(robotControls.robot.getPosition().x, 0);
			assert.equal(robotControls.robot.getPosition().y, 1);
			assert.equal(robotControls.robot.getFacing(), 'NORTH');
		});

		it('Calling RESET will reset the robot back to initial position and facing', () => {
			robotControls.addInstruction('PLACE 0,0,NORTH');
			robotControls.addInstruction('MOVE');
			assert.equal(robotControls.robot.getPosition().x, 0);
			assert.equal(robotControls.robot.getPosition().y, 1);
			robotControls.addInstruction('RESET');
			assert.equal(robotControls.addInstruction('REPORT'), 'undefined,undefined,undefined');
		});

	});

	describe('Robot Tests', () => {

		it('Robot test 1', () => {
			robotControls.addInstruction('PLACE 0,0,NORTH');
			robotControls.addInstruction('MOVE');
			assert.equal(robotControls.addInstruction('REPORT'), '0,1,NORTH');
		});

		it('Robot test 2', () => {
			robotControls.addInstruction('PLACE 0,0,NORTH');
			robotControls.addInstruction('LEFT');
			assert.equal(robotControls.addInstruction('REPORT'), '0,0,WEST');
		});

		it('Robot test 3', () => {
			robotControls.addInstruction('PLACE 1,2,EAST');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('LEFT');
			robotControls.addInstruction('MOVE');
			assert.equal(robotControls.addInstruction('REPORT'), '3,3,NORTH');
		});

		it('Robot test 4', () => {
			robotControls.addInstruction('PLACE 0,0,NORTH');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			assert.equal(robotControls.addInstruction('REPORT'), '0,4,NORTH');
		});

		it('Robot test 5', () => {
			robotControls.addInstruction('PLACE 0,0,WEST');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			assert.equal(robotControls.addInstruction('REPORT'), '0,0,WEST');
		});

		it('Robot test 6', () => {
			robotControls.addInstruction('PLACE 0,0,EAST');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			assert.equal(robotControls.addInstruction('REPORT'), '4,0,EAST');
		});

		it('Robot test 7', () => {
			robotControls.addInstruction('PLACE 0,0,SOUTH');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('LEFT');
			robotControls.addInstruction('MOVE');
			robotControls.addInstruction('MOVE');
			assert.equal(robotControls.addInstruction('REPORT'), '2,0,EAST');
		});

	});
});
