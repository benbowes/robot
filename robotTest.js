/*global window */
'use strict';

// This file is used to complile public/bundle.js which we use to
// test the toy robot app via the browser

import Room from './app/Room.js';
import Robot from './app/Robot.js';
import RobotControls from './app/RobotControls.js';

window.room = new Room([
	['a1', 'a2', 'a3', 'a4', 'a5'],
	['b1', 'b2', 'b3', 'b4', 'b5'],
	['c1', 'c2', 'c3', 'c4', 'c5'],
	['d1', 'd2', 'd3', 'd4', 'd5'],
	['e1', 'e2', 'e3', 'e4', 'e5']
]);
window.robot = new Robot(window.room);
window.robotControls = new RobotControls(window.robot);
